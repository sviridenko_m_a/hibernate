package com.epam.training.services;

import com.epam.training.DAO.DAO;
import com.epam.training.model.Project;

public class ProjectService {

    /**
     * Method to update project table by id.
     * @param dao - dao object
     * @param project_id - project id to update
     */
    public void updateProject(DAO<Object, Integer> dao, int project_id) {
        final Project result = (Project) dao.readProject(project_id);
        result.setProjectName("NeProject");
        dao.update(result);
    }

    /**
     * Method to delete project from DB table by id.
     * @param dao - dao object
     * @param project_id - project id to delete
     */
    public void removeProject(DAO<Object, Integer> dao, int project_id) {
        final Project project = (Project) dao.readProject(project_id);
        dao.delete(project);
    }
}
