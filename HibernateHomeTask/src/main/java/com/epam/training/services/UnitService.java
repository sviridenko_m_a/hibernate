package com.epam.training.services;

import com.epam.training.DAO.DAO;
import com.epam.training.model.Unit;

public class UnitService {

    /**
     * Method to update unit table by id.
     * @param dao - dao object
     * @param unit_id - unit id to update
     */
    public void updateUnit(DAO<Object, Integer> dao, int unit_id) {
        final Unit result = (Unit) dao.readUnit(unit_id);
        result.setUnitName("NewUnit");
        dao.update(result);
    }

    /**
     * Method to delete unit from DB table by id.
     * @param dao - dao object
     * @param unit_id - unit id to delete
     */
    public void removeUnit(DAO<Object, Integer> dao, int unit_id) {
        Unit unit = (Unit) dao.readUnit(unit_id);
        dao.delete(unit);
    }
}
