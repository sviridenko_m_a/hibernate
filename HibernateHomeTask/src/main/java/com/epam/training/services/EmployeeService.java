package com.epam.training.services;

import com.epam.training.DAO.DAO;
import com.epam.training.model.*;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployeeService {

    private final static Logger LOGGER = Logger.getLogger("LOGGER");
    private final SessionFactory factory;

    public EmployeeService(@NotNull final SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Method create & return information to put into unit table.
     */
    public static Unit getUnit() {
        Unit unit = new Unit();
        unit.setUnitName("javaDevelopers1");
        return unit;
    }

    /**
     * Method create & return information to put into address table.
     */
    public static Address getAddress() {
        Address address = new Address();
        address.setBuilding("142A");
        address.setStreet("Mazurova");
        return address;
    }

    /**
     * Method create & return information to put into employee & project tables.
     * @param unit - unit object
     * @param address - address object
     */
    public static Employee getEmployee(Unit unit, Address address) {
        Employee employee = new Employee();
        employee.setName("Vasiliev");
        employee.setEmployeeStatus(EmployeeStatus.Mr);
        employee.setExternal(false);
        employee.setUnit(unit);
        employee.setAddress(address);
        Set<Project> projects = new HashSet<>();
        Project project = new Project();
        Project project1 = new Project();
        project.setProjectName("Elevator");
        project1.setProjectName("Buss");
        projects.add(project);
        projects.add(project1);
        employee.setProjects(projects);
        return employee;
    }

    /**
     * Method create & return information to put into employeePersonalInfo table.
     * @param employee - employee object
     */
    public static EmployeePersonalInfo getEmployeePersonalInfo(Employee employee) {
        EmployeePersonalInfo employeePersonalInfo = new EmployeePersonalInfo();
        employeePersonalInfo.setEmployee(employee);
        employeePersonalInfo.setMaritalStatus("married");
        employeePersonalInfo.setEducation("highest");
        employeePersonalInfo.setTelephoneNumber("123-45-67");
        return employeePersonalInfo;
    }

    /**
     * Method to update employee table by id.
     * @param dao - dao object
     * @param employee_id - employee id to update
     */
    public void updateEmployee(DAO<Object, Integer> dao, int employee_id) {
        final Employee result = (Employee) dao.readEmployee(employee_id);
        result.setName("Vania");
        result.setExternal(true);
        result.setEmployeeStatus(EmployeeStatus.Ms);
        dao.update(result);
    }

    /**
     * Method to delete employee from DB table by id.
     * @param dao - dao object
     * @param employee_id - employee id to delete
     */
    public void removeEmployee(DAO<Object, Integer> dao, int employee_id) {
        Employee employee = (Employee) dao.readEmployee(employee_id);
        dao.delete(employee);
    }

    /**
     * Method add unit id to the employee table by id.
     * @param employee_id - employee_id
     * @param unit_id - unit_id
     */
    public void addEmployeeToUnit(int employee_id, int unit_id) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            Employee employee = session.load(Employee.class, employee_id);
            Unit unit = session.load(Unit.class, unit_id);
            employee.setUnit(unit);
            session.getTransaction().commit();
            session.close();
        }
    }

    /**
     * Method add project id to the employee table by id.
     * @param employee_id - employee_id
     * @param project_id - project_id
     */
    public void assignEmployeeForProject(int employee_id, int project_id) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            Employee employee = session.load(Employee.class, employee_id);
            Project project = session.load(Project.class, project_id);
            employee.getProjects().add(project);
            session.getTransaction().commit();
            session.close();
        }
    }

    /**
     * Method display all not external employees.
     */
    public void getNotExternalEmployee() {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("FROM Employee e where e.external=0");
            List result = query.list();
            for (Object o : result) {
                LOGGER.info(o);
            }
            session.getTransaction().commit();
            session.close();
        }
    }
}
