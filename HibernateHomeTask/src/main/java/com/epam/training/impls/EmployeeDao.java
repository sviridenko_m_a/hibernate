package com.epam.training.impls;

import com.epam.training.DAO.DAO;
import com.epam.training.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jetbrains.annotations.NotNull;

public class EmployeeDao implements DAO<Object, Integer> {

    private final SessionFactory factory;

    public EmployeeDao(@NotNull final SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Method open session to save object in DB.
     * @param object - object
     */
    @Override
    public void create(Object object) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
            session.close();
        }
    }

    /**
     * Method open session to read employee object by id.
     * @param id - id
     * @return - employ object
     */
    @Override
    public Object readEmployee(@NotNull final Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(Employee.class, id);
        }
    }

    /**
     * Method open session to read util object by id.
     * @param id - id
     * @return - util object
     */
    @Override
    public Object readUnit(@NotNull final Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(Unit.class, id);
        }
    }

    /**
     * Method open session to read project object by id.
     * @param id - id
     * @return - project object
     */
    @Override
    public Object readProject(@NotNull final Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(Project.class, id);
        }
    }

    /**
     * Method open session to update object.
     * @param object - object
     */
    @Override
    public void update(Object object) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(object);
            session.getTransaction().commit();
            session.close();
        }
    }

    /**
     * Method open session to delete object.
     * @param object - object
     */
    @Override
    public void delete(Object object) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(object);
            session.getTransaction().commit();
            session.close();
        }
    }
}
