package com.epam.training.DAO;

public interface DAO<Object, Key> {
    void create(Object object);
    Object readEmployee(Key key);
    Object readUnit(Key key);
    Object readProject(Key key);
    void update(Object object);
    void delete(Object object);
}
