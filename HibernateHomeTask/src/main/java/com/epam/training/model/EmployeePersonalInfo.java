package com.epam.training.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class EmployeePersonalInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String education;
    private String maritalStatus;

    @OneToOne()
    @JoinColumn(name = "employee_id")
    private Employee employee;

    private String telephoneNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        EmployeePersonalInfo that = (EmployeePersonalInfo) object;
        return id == that.id &&
                Objects.equals(education, that.education) &&
                Objects.equals(maritalStatus, that.maritalStatus) &&
                Objects.equals(employee, that.employee) &&
                Objects.equals(telephoneNumber, that.telephoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, education, maritalStatus, employee, telephoneNumber);
    }
}
