package com.epam.training.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "unit")
public class Unit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int unit_id;

    private String unitName;

    @OneToMany(mappedBy = "unit", orphanRemoval = true)
    private Set<Employee> employees = new HashSet<>();


    public int getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(int unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Unit unit = (Unit) object;
        return unit_id == unit.unit_id &&
                Objects.equals(unitName, unit.unitName) &&
                Objects.equals(employees, unit.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unit_id, unitName, employees);
    }

    @Override
    public String toString() {
        return "Unit{" +
                "unit_id=" + unit_id +
                ", unitName='" + unitName + '\'' +
                '}';
    }
}
