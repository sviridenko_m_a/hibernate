package com.epam.training;

import com.epam.training.impls.EmployeeDao;
import com.epam.training.model.*;
import com.epam.training.services.EmployeeService;
import com.epam.training.services.ProjectService;
import com.epam.training.services.UnitService;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Runner {

    public static void main(String[] args) {

        final Logger LOGGER = Logger.getLogger("LOGGER");

        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        SessionFactory sessionFactory = new MetadataSources(serviceRegistry)
                .buildMetadata()
                .buildSessionFactory();

        EmployeeDao dao = new EmployeeDao(sessionFactory);
        EmployeeService employeeService = new EmployeeService(sessionFactory);
        ProjectService projectService = new ProjectService();
        UnitService unitService = new UnitService();
        Unit unit = EmployeeService.getUnit();
        Address address = EmployeeService.getAddress();
        Employee employee = EmployeeService.getEmployee(unit, address);

        dao.create(unit);
        dao.create(address);
        dao.create(employee);
        dao.create(EmployeeService.getEmployeePersonalInfo(employee));

        LOGGER.info(dao.readEmployee(153));
        LOGGER.info(dao.readUnit(146));
        LOGGER.info(dao.readProject(78));

        employeeService.updateEmployee(dao, 150);
        unitService.updateUnit(dao, 134);
        projectService.updateProject(dao, 73);

        employeeService.addEmployeeToUnit(153, 140);
        employeeService.assignEmployeeForProject(153, 78);
        employeeService.getNotExternalEmployee();

        employeeService.removeEmployee(dao, 156);
        unitService.removeUnit(dao, 136);
        projectService.removeProject(dao, 69);
    }
}