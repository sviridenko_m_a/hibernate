package com.epam.training.services;

import com.epam.training.DAO.DAO;
import com.epam.training.impls.EmployeeDao;
import com.epam.training.model.Project;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProjectServiceTest {

    private SessionFactory factory;
    private DAO<Object, Integer> dao;
    ProjectService projectService = new ProjectService();

    @Test
    public void updateProject() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        projectService.updateProject(dao, 92);
        final Project result = (Project) dao.readProject(92);
        assertEquals("NeProject", result.getProjectName());
        factory.close();
    }

    @Test
    public void removeProject() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Project result = (Project) dao.readProject(121);
        assertNotNull(result);
        projectService.removeProject(dao, 121);
        final Project result1 = (Project) dao.readProject(121);
        assertNull(result1);
        factory.close();
    }
}