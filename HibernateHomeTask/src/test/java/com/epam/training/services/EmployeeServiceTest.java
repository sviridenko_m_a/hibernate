package com.epam.training.services;

import com.epam.training.DAO.DAO;
import com.epam.training.impls.EmployeeDao;
import com.epam.training.model.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

public class EmployeeServiceTest {

    private SessionFactory factory;
    private DAO<Object, Integer> dao;
    private EmployeeService employeeService = new EmployeeService(factory);

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void getUnit() {
        Unit unit = EmployeeService.getUnit();
        assertNotNull(unit);
        assertEquals("javaDevelopers1", unit.getUnitName());
    }

    @Test
    public void getAddress() {
        Address address = EmployeeService.getAddress();
        assertNotNull(address);
        assertEquals("Mazurova", address.getStreet());
    }

    @Test
    public void getEmployee() {
        Unit unit = new Unit();
        Address address = new Address();
        Employee employee = EmployeeService.getEmployee(unit, address);
        assertNotNull(employee);
        assertEquals("Vasiliev", employee.getName());
        assertEquals("Mr", employee.getEmployeeStatus().toString());
        assertFalse(employee.isExternal());
    }

    @Test
    public void getEmployeePersonalInfo() {
        Employee employee = new Employee();
        EmployeePersonalInfo employeePersonalInfo = EmployeeService.getEmployeePersonalInfo(employee);
        assertNotNull(employeePersonalInfo);
        assertEquals("highest", employeePersonalInfo.getEducation());
        assertEquals("married", employeePersonalInfo.getMaritalStatus());
        assertEquals("123-45-67", employeePersonalInfo.getTelephoneNumber());
    }

    @Test
    public void updateEmployee() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        employeeService.updateEmployee(dao, 164);
        final Employee result = (Employee) dao.readEmployee(164);
        assertEquals("Vania", result.getName());
        factory.close();
    }

    @Test
    public void removeEmployee() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Employee result = (Employee) dao.readEmployee(169);
        assertNotNull(result);
        employeeService.removeEmployee(dao, 169);
        final Employee result1 = (Employee) dao.readEmployee(169);
        assertNull(result1);
        factory.close();
    }

    @Test
    public void addEmployeeToUnit() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        employeeService.addEmployeeToUnit(148, 147);
        final Employee result = (Employee) dao.readEmployee(148);
        assertEquals(147, (int)result.getUnit().getUnit_id());
        factory.close();
    }

    @Test
    public void assignEmployeeForProject() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        employeeService.assignEmployeeForProject(148, 82);
        final Employee result = (Employee) dao.readEmployee(148);
        assertEquals("Buss", result.getProjects().toString());
        factory.close();
    }
}