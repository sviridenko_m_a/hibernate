package com.epam.training.services;

import com.epam.training.DAO.DAO;
import com.epam.training.impls.EmployeeDao;
import com.epam.training.model.Unit;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import static org.junit.Assert.*;

public class UnitServiceTest {

    private SessionFactory factory;
    private DAO<Object, Integer> dao;
    private UnitService unitService = new UnitService();

    @Test
    public void updateUnit() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        unitService.updateUnit(dao, 159);
        final Unit result = (Unit) dao.readUnit(159);
        assertEquals("NewUnit", result.getUnitName());
        factory.close();
    }

    @Test
    public void removeUnit() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Unit result = (Unit) dao.readUnit(170);
        assertNotNull(result);
        unitService.removeUnit(dao, 170);
        final Unit result1 = (Unit) dao.readUnit(170);
        assertNull(result1);
        factory.close();
    }
}