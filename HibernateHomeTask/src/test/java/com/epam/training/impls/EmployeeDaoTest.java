package com.epam.training.impls;

import com.epam.training.DAO.DAO;
import com.epam.training.model.*;
import com.epam.training.services.UnitService;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeDaoTest {

    private SessionFactory factory;
    private DAO<Object, Integer> dao;
    private UnitService unitService = new UnitService();

    @Test
    public void create() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        Unit unit = new Unit();
        unit.setUnitName("unitTest");
        dao.create(unit);
        final Unit result = (Unit) dao.readUnit(158);
        assertEquals("unitTest", result.getUnitName());
        factory.close();
    }

    @Test
    public void readEmployee() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Employee result = (Employee) dao.readEmployee(165);
        assertEquals("Vasiliev", result.getName());
        factory.close();
    }

    @Test
    public void readUnit() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Unit result = (Unit) dao.readUnit(159);
        assertEquals("NewUnit", result.getUnitName());
        factory.close();
    }

    @Test
    public void readProject() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Project result = (Project) dao.readProject(93);
        assertEquals("Buss", result.getProjectName());
        factory.close();
    }

    @Test
    public void update() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        unitService.updateUnit(dao, 137);
        final Unit result = (Unit) dao.readUnit(137);
        assertEquals("NewUnit", result.getUnitName());
        factory.close();
    }

    @Test
    public void delete() {
        factory = new Configuration().configure().buildSessionFactory();
        dao = new EmployeeDao(factory);
        final Unit result = (Unit) dao.readUnit(140);
        assertNotNull(result);
        dao.delete(result);
        final Unit result1 = (Unit) dao.readUnit(140);
        assertNull(result1);
        factory.close();
    }
}